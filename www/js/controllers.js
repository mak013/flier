  angular.module('starter.controllers', [])

/**
 * Our controller take data and functions 
 * from other files such as
 * services.js and set them to variables that our 
 * HTML pages will be able to access
 *
 */

 /**
  * Controllethat provides the functionality for 
  * the event detail page
  * -- Information
  * -- Calendar Sync 
  * -- Invite
  */
  .controller('EventDetailCtrl', function($scope, $stateParams, EventsAPI, $ionicModal, $cordovaCalendar, $cordovaSocialSharing) {
   // Load specific event
   $scope.event = EventsAPI.getEvent($stateParams.eventId);
   console.log($scope.event);

   // TODO
   // Use Cordova or Facebook API to create contacts
   $scope.contact = {
    name: 'Mittens Cat',
    info: 'Tap anywhere on the card to open the modal'
    }

  $scope.shareAnywhere = function() {
    $cordovaSocialSharing.share($scope.event['description'], $scope.event['event_name'], "www/imagefile.png", "http://blog.nraboy.com");
  }

  $ionicModal.fromTemplateUrl('contact-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal
  })  

  $scope.openModal = function() {
    $scope.modal.show()
  }

  $scope.closeModal = function() {
    $scope.modal.hide();
  };

  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });

  $scope.createEvent = function() {
    console.log("CREATING");
    var cal = window.plugins.calendar;
    var title = $scope.event['event_name'];
    var loc = $scope.event['location'];
    var notes = $scope.event['description'];
    var start = new Date(2015,0,1,20,0,0,0,0); // Jan 1st, 2015 20:00
    var end = new Date(2015,0,1,22,0,0,0,0);   // Jan 1st, 2015 22:00

    var onSuccess = function(message) {alert("Success: " + JSON.stringify(message))};
    var onError   = function(message) {alert("Error: " + message)};
    cal.createEventInteractively(title, loc, notes, start, end, onSuccess, onError);
  };

})

/**
 * Controller provides the list of events 
 * along with different sorting options 
 * Sorting by 
 *  -- Timestamp
 *  -- Distance
 *  -- Free Food
 */
 .controller('MapCtrl', function($scope, $http, $ionicLoading, Events, EventsAPI, EventsDistance) {
  $ionicLoading.show({
    template: 'loading'
  });

  // Integration of the EventsAPI from Kevin's API
  EventsAPI.get().then(
    function(data) {
      $scope.events = data;
      $scope.loading = false;
      console.log($scope.events);
      EventsDistance.calc($scope.events);

      window.localStorage['events'] = JSON.stringify(data);
      console.log('success', data);
      $ionicLoading.hide()

    }, 
    function(error) {
      $ionicLoading.hide()

      alert('Internet Connection Error', error);
      console.log('error', error);
      $scope.events = Events.all();
  });

  $scope.doRefresh = function() {
    EventsAPI.get().then(
      function(data) {
        $scope.events = data;
        $scope.loading = false;

        EventsDistance.calc($scope.events);

        window.localStorage['events'] = JSON.stringify(data);
        console.log('successful reload', data);
      },
      function(error) {
        $scope.events = [];
        console.log('error', error);
        alert('Internet Connection: ', error);
      }
    ).finally(function() {
       // Stop the ion-refresher from spinning
       $scope.$broadcast('scroll.refreshComplete');
    });
  };


  // Property on what we sort by
  $scope.sortDate = 'timeStampStart';
  // If the http call was made
  $scope.sortCall = false;

  // These move the div bar underneath the button bar
  // Also change the way we sort data or display
  $scope.moveLeft = function() {
    var closestButton = document.getElementById('moveline');

    $scope.sortDate = 'distance'

    move(closestButton)
    .ease('in-out')
    .set('margin-left', '60%')
    .duration('0.25s')
    .end();
  };

  $scope.moveRight = function() {
    var soonButton = document.getElementById('moveline');
    
    $scope.sortDate = 'event_name';

    move(soonButton)
    .ease('in-out')
    .set('margin-left', '10%')
    .duration('0.25s')
    .end();
  };

  // Provide us the google maps
  $scope.mapCreated = function(map) {
    $scope.map = map;
  }; 

  // Recenters the map to the user's coordinate
  $scope.centerOnMe = function () {
    console.log("Centering");
    if (!$scope.map) {
      return;
    }

    $scope.loading = $ionicLoading.show({
      content: 'Getting current location...',
      showBackdrop: false
    });

    navigator.geolocation.getCurrentPosition(function (pos) {
      console.log('Got pos', pos);
      $scope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
      $scope.loading.hide();
    }, function (error) {
      alert('Unable to get location: ' + error.message);
    });
  };

})

.controller('AccountCtrl', function($scope, $http, EventsDistance, $ionicPopup) {
  /*  Using Google to find the distance between two points,
      origin: the cordinates of the user
      destinations: Use the name of the address, Google will translate it to coordinates
      mode: Driving, Walking
      key: Use this: AIzaSyAibJZienLbE1pyPKgCHKAgsP_UZtEzk6M
   */


})

.controller('EventMapCtrl', function($scope) {

  // Provide us the google maps
  $scope.mapCreated = function(map) {
    $scope.map = map;
  }; 

  // Recenters the map to the user's coordinate
  $scope.centerOnMe = function () {
    console.log("Centering");
    if (!$scope.map) {
      return;
    }

    $scope.loading = $ionicLoading.show({
      content: 'Getting current location...',
      showBackdrop: false
    });

    navigator.geolocation.getCurrentPosition(function (pos) {
      console.log('Got pos', pos);
      $scope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
      $scope.loading.hide();
    }, function (error) {
      alert('Unable to get location: ' + error.message);
    });
  };
})

.controller('LoginCtrl', function(LoginService, $scope, $ionicPopup, $state,
  $ionicLoading, validResponse) {
    $scope.data = {};
    $ionicLoading.hide();
    $scope.login=function(){
      $ionicLoading.show();
      LoginService.login($scope.data.email,$scope.data.accessCode).then(function(data) {
        console.log(data.data['msg']);
        if(validResponse.valid(data.data['msg'])) {
          $state.go('tab.events');
          window.localStorage['login'] = true;
          $ionicLoading.hide();
        }
        else {
          var alertPopup = $ionicPopup.alert({
            title: 'Login failed!',
            template: 'Please check your credentials!'
          });
          $ionicLoading.hide();
        }
      }, function(err) {
        $ionicLoading.hide();
        console.log(err);        
      });

    }
})

.controller('SignCtrl', function($scope, SignupService, $ionicPopup, 
  $state, $ionicLoading, valid_domains) {
    $scope.data = {};
    $ionicLoading.hide();
    if(String(window.localStorage['login']) === "true"){
      $state.go('tab.events');
    }
    $scope.signup = function() {
      $ionicLoading.show();
      if(validateEmail($scope.data.email)) {
        SignupService.signup($scope.data.email).then(function(data) {
            $state.go('login');
            $ionicLoading.hide();
        },  function(error) {
              $ionicLoading.hide();
              var alertPopup = $ionicPopup.alert({
                  title: 'Login failed!',
                  template: 'Please check your credentials!'
            });
        });
      }else {
        var alertPopup = $ionicPopup.alert({
          title: 'Login failed!',
          template: 'Please check your credentials!'
        });
        $ionicLoading.hide();
      }

    }

    var validateEmail = function(email) {
      var numAt=0;
      var index; 
      for(var i=0;i<email.length; i++){
        if(email.charAt(i)=='@'){
          index=i;
          numAt++;
        }
      }
      var domain=email.substring(index+1);
      return valid_domains.indexOf(domain)!=-1 && numAt==1;
    }
});;