angular.module('starter.directives', [])

/**
 * I'm guessing we use this when were connecting to
 * different APIs
 *
 */

.directive('map', function() {
  return {
    restrict: 'E',
    scope: {
      onCreate: '&'
    },
    link: function ($scope, $element, $attr) {

      var options = { timeout: 30000, enableHighAccuracy: true, maximumAge: 10000 };

      navigator.geolocation.getCurrentPosition(function (pos) {
        var myLatlng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
        var LatLng = new google.maps.LatLng(32.881190, -117.237553);

        var styleArray = [{
          featureType: "all",
          stylers: [
            { saturation: -80 }
          ]
        },
        {
          featureType: "transit.station.bus",
          stylers: [{ visibility: "off" }]
        },{
          featureType: "poi.business",
          elementType: "labels",
          stylers: [{ 
            visibility: "off" }
          ]}
        ];

        var mapOptions = {
          center: myLatlng,
          disableDefaultUI: true,
          zoom: 16,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        
        var map = new google.maps.Map($element[0], mapOptions);

        map.setOptions({styles: styleArray});

        map.setOptions({scaleControl: false});

        
        var marker = new google.maps.Marker({
          position: myLatlng,
          map: map,
          title: 'Hello World!'
        });

        map.addListener('center_changed', function() {
          // 3 seconds after the center of the map has changed, pan back to the
          // marker.
          window.setTimeout(function() {
          //   var data = map.getCenter();
          //   console.log(data);
          }, 3000);

          marker.setPosition(map.getCenter());


        });


        $scope.onCreate({map: map});

        // Stop the side bar from dragging when mousedown/tapdown on the map
        google.maps.event.addDomListener($element[0], 'mousedown', function (e) {
          e.preventDefault();
          return false;
        });

      }, function (error) {
        alert('Unable to get location: ' + error.message);
      }, options);


      if (document.readyState === "complete") {
        initialize();
      } else {
        google.maps.event.addDomListener(window, 'load', initialize);
      }
    }
  }
})

.directive('ionSearch', function() {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                getData: '&source',
                model: '=?',
                search: '=?filter'
            },
            link: function(scope, element, attrs) {
                attrs.minLength = attrs.minLength || 0;
                scope.placeholder = attrs.placeholder || '';
                scope.search = {value: ''};

                if (attrs.class)
                    element.addClass(attrs.class);

                if (attrs.source) {
                    scope.$watch('search.value', function (newValue, oldValue) {
                        if (newValue.length > attrs.minLength) {
                            scope.getData({str: newValue}).then(function (results) {
                                scope.model = results;
                            });
                        } else {
                            scope.model = [];
                        }
                    });
                }

                scope.clearSearch = function() {
                    scope.search.value = '';
                };
            },
            template: '<div class="item-input-wrapper">' +
                        '<i class="icon ion-android-search"></i>' +
                        '<input type="search" placeholder="{{placeholder}}" ng-model="search.value">' +
                        '<i ng-if="search.value.length > 0" ng-click="clearSearch()" class="icon ion-close"></i>' +
                      '</div>'
        };
});